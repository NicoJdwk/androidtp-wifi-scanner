package com.jendro.wifi_scanner;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

public class SecondFragment extends Fragment {

    private Canvas mCanvas;
    private Paint mPaint = new Paint();
    private Paint mPaintText = new Paint(Paint.UNDERLINE_TEXT_FLAG);
    private Bitmap mBitmap;
    private ImageView mImageView;
    private int mColorCircles;
    private int backgroundColor;



    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        // Retrieve colors
        mColorCircles = ResourcesCompat.getColor(getResources(),
                R.color.colorCircles, null);

        backgroundColor = ResourcesCompat.getColor(getResources(),
                R.color.colorBackground, null);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get imageView
        mImageView = (ImageView) view.findViewById(R.id.myimageview);

        // When view loaded and sized --> Create and draw on Canvas
        view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                createMultipleCircles(v, 3);
            }
        });


        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(SecondFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }

    /*
        Create a Canvas linked to an ImageView
        --> Draw multiple circles from center of canvas to different radius repartited accross Canvas size


        ex : if canvas width = 200px; number = 2
        Step between circles = 200 / 2 / 2 = 50px
        --> First circle radius = 50 * 1 = 50px
        --> Second circle radius = 50 * 2  = 100px;

        Args:
            - View: view on which the ImageView is
            - number: Number of circles to be drawned
     */
    public void createMultipleCircles(View view, int number) {
        //  Get size of Canvas
        int vWidth = view.getRight();
        int vHeight = view.getHeight();

        // Calculate steps between circles
        int step = vWidth / 2 / number;

        //  Create bitmap and associate it with ImagevIew
        mBitmap = Bitmap.createBitmap(vWidth, vHeight, Bitmap.Config.ARGB_8888);
        mImageView.setImageBitmap(mBitmap);

        mCanvas = new Canvas(mBitmap);


        // Draw circles
        for(int i = number; i > 0; i--) {
            createCircle(mCanvas, vWidth / 2, vHeight / 2, step * i);
        }

        // Refresh view to redraw Canvas
        view.invalidate();
    }

    /*
        Simply creates a circle with mColorCircles for border and fill with mBackgroundColor
     */
    private void createCircle(Canvas canvas, int width, int height, int radius) {

        mPaint.setColor(mColorCircles);

        canvas.drawCircle(width ,height ,radius, mPaint);

        mPaint.setColor(backgroundColor);

        canvas.drawCircle(width,height,radius -10, mPaint);

    }
}
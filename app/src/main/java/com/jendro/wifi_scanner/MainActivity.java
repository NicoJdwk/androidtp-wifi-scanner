package com.jendro.wifi_scanner;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
    Author: Nicolas JENDROWIAK
    Description:
        This class initialize the UI + Handle wifi networks scan
        It creates a WifiManager object to start a scan
        An intent is triggered when result is available

        Doc about wifiManager : https://developer.android.com/guide/topics/connectivity/wifi-scan#java
        
 */

public class MainActivity extends AppCompatActivity {

    private WifiScanResultReceiver wifiScanResultReceiver;
    public List<ScanResult> wifiList;

    WifiManager mWifiManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //  Start scan when user click on round button
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mWifiManager.startScan()) {
                    Toast.makeText(MainActivity.this, "scanning", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(MainActivity.this, "scan failed", Toast.LENGTH_SHORT).show();
                }
            }
        });


        //  Set event wifi
        this.wifiScanResultReceiver = new WifiScanResultReceiver();
        IntentFilter filter = new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        registerReceiver(this.wifiScanResultReceiver, filter);

        mWifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        //  Scan networks
        if(mWifiManager.startScan()) {
            Toast.makeText(this, "scanning", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, "scan failed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        //  Ask for permission at runtime
        //  --> Needed for >= Android 6.0
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if(checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 87);
            }

            if(checkSelfPermission(Manifest.permission.CHANGE_WIFI_STATE) != PackageManager.PERMISSION_GRANTED)
            {
                requestPermissions(new String[]{Manifest.permission.CHANGE_WIFI_STATE}, 87);
            }

            if(checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 87);
            }
        }
    }

    //  Get wifi scan result --> Used when coming from secondFragment back to first fragment
    public List<ScanResult> GetResults() {
        return wifiList;
    }


    //  Triggered when results from ScanResults are available
    class WifiScanResultReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            //  Get results
            wifiList = mWifiManager.getScanResults();

            //  Trigger Intent to pass data to Fragments
            Intent retIntent = new Intent(FirstFragment.REC_DATA);
            retIntent.putExtra("data",(Serializable) wifiList);
            sendBroadcast(retIntent);
        }
    }
}
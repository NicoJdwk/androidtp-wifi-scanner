package com.jendro.wifi_scanner;
// https://stackoverflow.com/questions/18741034/how-to-get-available-wifi-networks-and-display-them-in-a-list-in-android
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import java.util.ArrayList;
import java.util.List;

import static androidx.core.content.ContextCompat.getSystemService;
import static androidx.core.content.ContextCompat.startForegroundService;

public class FirstFragment extends Fragment {
    List<ScanResult> scanResultsList;
    ArrayList<String> wifiListString;
    ListView list_wifi;

    protected DataReceiver dataReceiver;
    public static final String REC_DATA = "REC_DATA";

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //  Register Intent to get data from MainActivity when Wifi scan is complete
        DataReceiver Receiver = new DataReceiver();
        IntentFilter filter = new IntentFilter(REC_DATA);
        getActivity().registerReceiver(Receiver, filter);


        //  Retrieve listview to display wifi network
        list_wifi = view.findViewById(R.id.list_wifi);
        wifiListString = new ArrayList<String>();

        //  Display toast message when use hits element in ListView
        // --> Display dB of selected network
        list_wifi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), "dB: " + scanResultsList.get(position).level, Toast.LENGTH_SHORT).show();
            }
        });

        //  Handle 'PREVIOUS' button click
        view.findViewById(R.id.button_first).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(FirstFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment);
            }
        });
    }

    public void WifiListRefreshed(List<ScanResult> listResult){
        wifiListString.clear();

        for (ScanResult res:listResult
             ) {
            wifiListString.add(res.SSID);
        }
        list_wifi.setAdapter(new ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, wifiListString));
    }

    //  Action to make when ScanResults are received from MainActivity
    private class DataReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            List<ScanResult> list = (List<ScanResult>) intent.getSerializableExtra("data");
            scanResultsList = list;

            // Do anything including interact with your UI
            WifiListRefreshed(list);
        }
    }


}